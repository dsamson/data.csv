package data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * 
 * @author Daniel Samson
 * @data 3 November 2012.
 * @version 1
 */
public class CSV {
    public CSV() {
        data = new ArrayList<Object[]>();
    }
    
 /**
  * 
  * @param filePath
  * @throws FileNotFoundException
  * @throws IOException
  * @throws InconsistentDataException 
  * when the columnTotal of each row is different.
  */   
    public ArrayList<Object[]> readFromFile(String filePath) throws 
            FileNotFoundException, 
            IOException, 
            Exception {
        
        if (!data.isEmpty()) {
            data.clear(); // House keeping
        }
        
        /**
         * Load data from file.
         */
        bufferedReader = new BufferedReader(new FileReader(filePath));
        String line = "noData";
        String values[];
        
        try {
            while (line != null) {
                line = bufferedReader.readLine();
                values = line.split(",");
                data.add(values);
            }
        } finally {
            bufferedReader.close();
        }
        
        /**
         * Validate CSV
         */
        for (int i =0; i < data.size(); i++) {
            Object[] row = data.get(i);
            if (row.length != data.get(0).length) {
                throw new Exception("InconsistentDataException");
            }
        }
        return data;
    }
    
    /**
     * 
     * @param filePath
     * @throws NoDataException 
     */
    public void writeToFile (String filePath) throws Exception {
        if (data.isEmpty()) {
            throw new Exception("NoDataException");
        }
        
        bufferedWriter = new BufferedWriter(new FileWriter(filePath));
        String line = "noData";
        Object[] values;
        
        try {
            for (int i = 0; i < data.size(); i++) {
                values = data.get(i);
                for (int ii = 0; ii < data.get(i).length; ii++) {
                    line += values[ii].toString();
                    if (!(ii < values.length - 1)) {
                        line += ","; // add "," when !lastValue 
                    }
                }
                bufferedWriter.write(line);
                bufferedWriter.newLine();
            }
        } finally {
            bufferedReader.close();
        }
    }
    
    /**
     * 
     * @param rowIndex
     * @return Entire row as Object[].
     */
    public Object[] readAt (int rowIndex) {
        return data.get(rowIndex);
    }
    
    /**
     * 
     * @param rowIndex
     * @param columnIndex
     * @return Cell as Object.
     */
    public Object readAt(int rowIndex, int columnIndex) {
        Object[] row = data.get(rowIndex);
        return row[columnIndex];
    }
    
    public void writeAt(int rowIndex, Object[] rowData) {
        data.get(rowIndex).equals(rowData);
    }
    
    public void writeAt (int rowIndex, int columnIndex, Object cellData) {
        Object[] row = data.get(rowIndex);
        row[columnIndex] = cellData;
    }
    /**
     * 
     * @return true if container is empty.
     */
    public boolean isEmpty() {
        return data.isEmpty();
    }
    
    /**
     * Force instance to clear data.
     */
    public void empty() {
        data.clear();
    }
    
    /**
     * 
     * @return Number of rows.
     */
    public int totalRows() {
        return data.size();
    }
    
    /**
     * 
     * @return  Number Of Columns
     */
    public int totalColumns() {
        Object[] result = data.get(0);
        return result.length;
    }
    
    /**
     * Adds a new blank row to the table.
     * @throws NoDataException
     */
    public void addRow() throws Exception {
        if(isEmpty()) {
            throw new Exception("NoDataException");
        } else {
            Object[] firstRow = data.get(0);
            data.add(new Object[firstRow.length]);
        }
    }
    
    /**
     * 
     * Collection that holds the CSV data in a table format.
     */
    private ArrayList<Object[]> data;
    
    /**
     * File variables
     */
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;
    
}
